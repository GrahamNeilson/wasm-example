#[no_mangle]
pub extern fn add_one(x: u32) -> u32 {
    x + 1
}

#[no_mangle]
pub extern fn add_two(x: u32) -> u32 {
    x + 2
}

#[no_mangle]
// unsigned, u8:  0...255
// signed. i8: -127 - 128
pub extern fn subtract_one(x: i8) -> i8 { // 8bits
    x - 1
}

#[no_mangle]
pub extern fn square(x: u32) -> u32 {
    x * x
}
