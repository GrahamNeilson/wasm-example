# WASM-Example

A simple Webpack/CycleJs application uitlising Rust compiled to WASM helper functions.
```
git clone git@gitlab.com:GrahamNeilson/wasm-example.git
npm i
npm run dev
```

## Compiliing Rust to WASM

rustup is an installer for the systems programming language Rust

Rust is a new language relative to its predecessors (most importantly C, which preceded it by 38 years), but its genealogy creates its multiparadigm approach. Rust is considered a C-like language, but the other features it includes create advantages over its predecessors (see Figure 1).

First, Rust is heavily influenced by Cyclone (a safe dialect of C and an imperative language), with some aspects of object-oriented features from C++. But, it also includes functional features from languages like Haskell and OCaml. The result is a C-like language that supports multiparadigm programming (imperative, functional, and object oriented).

Rustup gives you easy access to the nightly compiler and its experimental features

```
curl https://sh.rustup.rs -sSf | sh
```

What is a _systems programming language_?

A system programming language usually refers to a programming language used for system programming; such languages are designed for writing system software, which usually requires different development approaches when compared with application software.

Cargo's bin directory ($HOME/.cargo/bin) will be added to your PATH
environment variable. But in order to get going without having to restart, we need to run:

```
source $HOME/.cargo/env
```

Once we have rust on our system, we want to set the nightly toolchain as a default. Only the nightly toolchain currently supports wasm

```
rustup default nightly
```

Then we specify target platform - since wasm is not compiled against any specific platform, the last two values are unknown/unknown. I'm not sure why we need this, given when we do a Cargo build we need to specify the target platform there - perhaps this is the default.

```
rustup target add wasm32-unknown-unknown
```

Use Cargo - the rust package manager - to install wasm-pack. wasm pack helps with rust generated / generates wasm (unused). Described as a one stop shop for wasm helpers - we won’t be using this yet, but it's recommomended to have a look at.

```
cargo install wasm-pack
```

Install wasm-gc, used to remove all unused exports, imports, etc from a wasm file - our compiled output is run through this and cleaned.

```
cargo install wasm-gc
```

Within the WebApp in which your WASM will be consumed, create new rust project using cargo (—lib denotes library) at the root of your project (or anywhere, but there commands in the package file are set up for this location)

```
cargo new --lib utils
```

Add this to the end of the ```Cargo.toml``` file

(https://doc.rust-lang.org/reference/linkage.html)

```
[lib]
crate-type = ["cdylib"]
```

Add example public functions in ```src/lib.rs```, replacing the default that is created

We have to add a new attribute, no_mangle. When you create a Rust library, it changes the name of the function in the compiled output. The reasons for this are outside the scope of this example, but in order for other languages to know how to call the function, we can’t do that. This attribute turns that behavior off.

```
#[no_mangle]
pub extern fn add_one(x: u32) -> u32 {
  x + 1
}

#[no_mangle]
pub extern fn subtract_one(x: u32) -> u32 {
  x - 1
}

#[no_mangle]
pub extern fn square(x: u32) -> u32 {
  x * x
}
```

## Compile the Rust library

```
cargo  +nightly build --manifest-path=utils/Cargo.toml --target wasm32-unknown-unknown --release
```

Clean the compiled output, placing the ouptut in the root of the ```/utils``` folder

```
wasm-gc utils/target/wasm32-unknown-unknown/release/utils.wasm -o utils/utils.gc.wasm
```

## Compile the Rust library from the Rust project (utils)
i.e. no manifest option, which is otherwise required to point to the Cargo manifest

```
cargo  +nightly build --target wasm32-unknown-unknown --release
```

Clean the compiled output

```
wasm-gc target/wasm32-unknown-unknown/release/utils.wasm -o utils.gc.wasm
```

## Rust Playground
play.rust-lang.org



