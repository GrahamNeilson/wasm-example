import xs, { Stream } from 'xstream'
import { div, VNode} from '@cycle/dom'

const getUtils = () => import('../../utils/utils.gc.wasm')
// const getUtils = () => import('../../utils/utils') // legacy (possibly compiled from rust / CI)

interface Sinks {
	DOM: Stream<VNode>
}

function App(): Sinks {

	const vdom$ =
		xs.fromPromise(getUtils())
			.debug(console.log)
			.map(module => ({
				add_one: module.add_one(3),
        square: module.square(3),
        add_two: module.add_two(3),
			}))
			.map(values =>
				div({
					style: {
						padding: '24px',
						fontSize: '32px',
						fontFamily: 'monospace',
					}
				}, JSON.stringify(values))
			)

	return {
    DOM: vdom$,
	}
}

export default App
